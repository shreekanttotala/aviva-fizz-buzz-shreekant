import React, { Component } from 'react';
import FizzBuzz from './components/fizzBuzz/FizzBuzz';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            Welcome Fizz-Buzz
          </p>
        </header>
        <FizzBuzz />
      </div>
    );
  }
}

export default App;
