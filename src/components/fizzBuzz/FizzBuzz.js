import React, { Component } from 'react';
import './FizzBuzz.css';

class FizzBuzz extends Component {
    constructor(props) {
      super(props);
        this.state = {
          limit:20,
          inputValue:'',
          list: []
        };

        this.handleOnChange = this.handleOnChange.bind(this);
        this.getList = this.getList.bind(this);
        this.nextPage = this.nextPage.bind(this);
        this.previousPage = this.previousPage.bind(this);
        this.validate = this.validate.bind(this);
        this.currentPage = 1;
        this.lastPage = 1;
        this.error = false;
    }

  handleOnChange(event) {
    if(event.target.value === '') this.error = false;
    this.setState({inputValue:event.target.value});
  }

  getWeekDay(){
    let d = new Date();
    let weekday = new Array(7);
    weekday[0] =  "S";
    weekday[1] = "M";
    weekday[2] = "T";
    weekday[3] = "W";
    weekday[4] = "T";
    weekday[5] = "F";
    weekday[6] = "S";

    return weekday[d.getDay()];
  }


renderList(){
  let { list } = this.state;
    let day = this.getWeekDay();
    return list.map(item => {
      return (
        <li key={item}>
          <span className="fizz-buzz-list">
		  {(item%3) === 0 && <span className="fizz">{day + "izz"} </span>}
          {(item%5) === 0 && <span className="buzz">{day + "uzz"}</span>}
          {(item%3) !== 0 && (item%5) !== 0 && <span className="default">{item}</span>}
		  </span>
        </li>
      );
    });
  }

  validate(value) {
    if(isNaN(value) || value <= 0  || value > 1000) {
      this.error = true;
      return;
    }
    this.error = false;
  }

  getList(evt, retainCurrentPage) {
    let {inputValue, limit } = this.state;
    if(!retainCurrentPage) {
      this.currentPage = 1;
      this.lastPage = 1;
      this.validate(inputValue);
    }
    let list=[];
    if(!this.error) {
    let start = ((this.currentPage -1) * limit) + 1;
    let end = (this.currentPage * limit);
      if(inputValue !== '') {
        if(inputValue < end)
          end = inputValue;
        this.lastPage = Math.ceil(inputValue/limit);
        for(let i=start;i<=end;i++) {
          list.push(i);
        }
      }
    }
   this.setState({list}); 
   }

    nextPage(evt) {
      this.currentPage = this.currentPage + 1
     this.getList(evt, true);
    }

    previousPage(evt) {
      this.currentPage = this.currentPage -1
      this.getList(evt, true);
    }


  render() {
    const { inputValue } = this.state;
    let prevbtn_disabled = this.currentPage === 1 ? true : false;
    let nextbtn_disabled = this.currentPage === this.lastPage ? true : false;
    let gobtn_disabled = inputValue === "" ? true : false;
    return (
      <div className="fizz-buzz">
        <input className="input" type="text" name="num" ref='inputText' value={inputValue} onChange={this.handleOnChange} />
        <input type="button" className="button" onClick={this.getList} value="Go" disabled={gobtn_disabled} />
        {this.error && <div className="error">Allow positive integer between 1 and 1000.</div>}
        <div className="pagination">
          <input type="button" className="button" onClick={this.previousPage} value="Previous" disabled={prevbtn_disabled} />
          <input type="button" className="button" onClick={this.nextPage} value="Next" disabled={nextbtn_disabled} />
        </div>
        <div className="list">
          <ul>
          {this.renderList()}
          </ul>
        </div>
      </div>
    );
  }

}

export default FizzBuzz;
